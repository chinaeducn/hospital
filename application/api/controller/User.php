<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\library\Ems;
use app\common\library\Sms;
use fast\Random;
use think\Validate;
header('Access-Control-Allow-Origin:*');
/**
 * 会员接口
 */
class User extends Common
{
    protected $noNeedLogin = ['index','login', 'mobilelogin', 'register', 'resetpwd', 'changeemail', 'changemobile', 'third', 'sendCaptcha','logList'];
    protected $noNeedRight = '*';
    public function _initialize()
    {

        parent::_initialize();

    }

    /**
     * 会员中心
     */
    public function index()
    {
        $user=$this->auth->getUser();
        if(!$user){
            $this->outputError('请登录');
        }
        $uid= $user->id;
        $info=[];
        $info['username']=$user->username;
        $info['mobile']=$user->mobile;
        $info['money']=$user->money;
        //今日新增用户
        $info['today_users']= Db('user')->where(['jointime'=>['gt',strtotime(date('Y-m-d').'00:00:00')],'fromid'=>$uid])->count();
        $info['total_users']= Db('user')->where(['fromid'=>$uid])->count();
        $info['total_money']=Db('bonus_log')->where(['type'=>2,'uid'=>$uid])->sum('bonus');
        $this->outputData($info);
    }

    /**
     * 会员登录
     *
     * @param string $account 账号
     * @param string $password 密码
     */
    public function login()
    {
        $account = $this->request->request('account');
        $password = $this->request->request('password');
        if (!$account) {
            $this->error('请填写手机号');
        }
        if (!$password) {
            $this->error('请填写密码');
        }
        $ret = $this->auth->login($account, $password);
        if ($ret) {
            $data = ['userinfo' => $this->auth->getUserinfo()];
            $this->success(__('Logged in successful'), $data);
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 手机验证码登录
     *
     * @param string $mobile 手机号
     * @param string $captcha 验证码
     */
    public function mobilelogin()
    {
        $mobile = $this->request->request('mobile');
        $captcha = $this->request->request('captcha');
        if (!$mobile || !$captcha) {
            $this->error(__('Invalid parameters'));
        }
        if (!Validate::regex($mobile, "^1\d{10}$")) {
            $this->error(__('Mobile is incorrect'));
        }
        if (!Sms::check($mobile, $captcha, 'mobilelogin')) {
            $this->error(__('Captcha is incorrect'));
        }
        $user = \app\common\model\User::getByMobile($mobile);
        if ($user) {
            if ($user->status != 'normal') {
                $this->error(__('Account is locked'));
            }
            //如果已经有账号则直接登录
            $ret = $this->auth->direct($user->id);
        } else {
            $ret = $this->auth->register($mobile, Random::alnum(), '', $mobile, []);
        }
        if ($ret) {
            Sms::flush($mobile, 'mobilelogin');
            $data = ['userinfo' => $this->auth->getUserinfo()];
            $this->success(__('Logged in successful'), $data);
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 注册会员
     *
     * @param string $username 用户名
     * @param string $password 密码
     * @param string $email 邮箱
     * @param string $mobile 手机号
     * @param string $code 验证码
     */
    public function register()
    {
        $username = $this->request->request('username');
        $password = $this->request->request('password');
        $idcard = $this->request->request('idcard');
        $mobile = $this->request->request('mobile');
        $code = $this->request->request('captcha');
        $fromid = $this->request->request('fromid');

        if (!$username ) {
            $this->error('请填写手机号');
        }
        if (!$password||strlen($password)<6) {
            $this->error('请填写密码或密码太短');
        }
        if (!$idcard) {
            $this->error('请填写身份证信息');
        }
        if ($mobile && !Validate::regex($mobile, "^1\d{10}$")) {
            $this->error(__('Mobile is incorrect'));
        }
        $ret = Sms::check($mobile, $code, 'register');
        if (!$ret) {
            $this->error(__('Captcha is incorrect'));
        }
        $ret = $this->auth->register($username, $password, $idcard,$fromid, $mobile, []);
        if ($ret) {
            $data = ['userinfo' => $this->auth->getUserinfo()];
            $this->success(__('Sign up successful'), $data);
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 注销登录
     */
    public function logout()
    {
        $this->auth->logout();
        $this->success(__('Logout successful'));
    }

    /**
     * 修改会员个人信息
     *
     * @param string $avatar 头像地址
     * @param string $username 用户名
     * @param string $nickname 昵称
     * @param string $bio 个人简介
     */
    public function profile()
    {
        $user = $this->auth->getUser();
        $username = $this->request->request('username');
        $nickname = $this->request->request('nickname');
        $bio = $this->request->request('bio');
        $avatar = $this->request->request('avatar', '', 'trim,strip_tags,htmlspecialchars');
        if ($username) {
            $exists = \app\common\model\User::where('username', $username)->where('id', '<>', $this->auth->id)->find();
            if ($exists) {
                $this->error(__('Username already exists'));
            }
            $user->username = $username;
        }
        $user->nickname = $nickname;
        $user->bio = $bio;
        $user->avatar = $avatar;
        $user->save();
        $this->success();
    }

    /**
     * 修改邮箱
     *
     * @param string $email 邮箱
     * @param string $captcha 验证码
     */
    public function changeemail()
    {
        $user = $this->auth->getUser();
        $email = $this->request->post('email');
        $captcha = $this->request->request('captcha');
        if (!$email || !$captcha) {
            $this->error(__('Invalid parameters'));
        }
        if (!Validate::is($email, "email")) {
            $this->error(__('Email is incorrect'));
        }
        if (\app\common\model\User::where('email', $email)->where('id', '<>', $user->id)->find()) {
            $this->error(__('Email already exists'));
        }
        $result = Ems::check($email, $captcha, 'changeemail');
        if (!$result) {
            $this->error(__('Captcha is incorrect'));
        }
        $verification = $user->verification;
        $verification->email = 1;
        $user->verification = $verification;
        $user->email = $email;
        $user->save();

        Ems::flush($email, 'changeemail');
        $this->success();
    }

    /**
     * 修改手机号
     *
     * @param string $email 手机号
     * @param string $captcha 验证码
     */
    public function changemobile()
    {
        $user = $this->auth->getUser();
        $mobile = $this->request->request('mobile');
        $captcha = $this->request->request('captcha');
        if (!$mobile || !$captcha) {
            $this->error(__('Invalid parameters'));
        }
        if (!Validate::regex($mobile, "^1\d{10}$")) {
            $this->error(__('Mobile is incorrect'));
        }
        if (\app\common\model\User::where('mobile', $mobile)->where('id', '<>', $user->id)->find()) {
            $this->error(__('Mobile already exists'));
        }
        $result = Sms::check($mobile, $captcha, 'changemobile');
        if (!$result) {
            $this->error(__('Captcha is incorrect'));
        }
        $verification = $user->verification;
        $verification->mobile = 1;
        $user->verification = $verification;
        $user->mobile = $mobile;
        $user->save();

        Sms::flush($mobile, 'changemobile');
        $this->success();
    }

    /**
     * 第三方登录
     *
     * @param string $platform 平台名称
     * @param string $code Code码
     */
    public function third()
    {
        $url = url('user/index');
        $platform = $this->request->request("platform");
        $code = $this->request->request("code");
        $config = get_addon_config('third');
        if (!$config || !isset($config[$platform])) {
            $this->error(__('Invalid parameters'));
        }
        $app = new \addons\third\library\Application($config);
        //通过code换access_token和绑定会员
        $result = $app->{$platform}->getUserInfo(['code' => $code]);
        if ($result) {
            $loginret = \addons\third\library\Service::connect($platform, $result);
            if ($loginret) {
                $data = [
                    'userinfo' => $this->auth->getUserinfo(),
                    'thirdinfo' => $result
                ];
                $this->success(__('Logged in successful'), $data);
            }
        }
        $this->error(__('Operation failed'), $url);
    }

    /**
     * 重置密码
     *
     * @param string $mobile 手机号
     * @param string $newpassword 新密码
     * @param string $captcha 验证码
     */
    public function resetpwd()
    {
        $type = $this->request->request("type");
        $mobile = $this->request->request("mobile");
        $email = $this->request->request("email");
        $newpassword = $this->request->request("newpassword");
        $captcha = $this->request->request("captcha");
        if (!$newpassword || !$captcha) {
            $this->error('请输入验证码');
        }
        if ($type == 'mobile') {
            if (!Validate::regex($mobile, "^1\d{10}$")) {
                $this->error('手机号格式不正确');
            }
            $user = \app\common\model\User::getByMobile($mobile);
            if (!$user) {
                $this->error('用户不存在');
            }
            $ret = Sms::check($mobile, $captcha, 'resetpwd');
            if (!$ret) {
                $this->error('验证码已过期');
            }
            Sms::flush($mobile, 'resetpwd');
        } else {
            if (!Validate::is($email, "email")) {
                $this->error(__('Email is incorrect'));
            }
            $user = \app\common\model\User::getByEmail($email);
            if (!$user) {
                $this->error('用户不存在');
            }
            $ret = Ems::check($email, $captcha, 'resetpwd');
            if (!$ret) {
                $this->error(__('验证码已过期'));
            }
            Ems::flush($email, 'resetpwd');
        }
        //模拟一次登录
        $this->auth->direct($user->id);
        $ret = $this->auth->changepwd($newpassword, '', true);
        if ($ret) {
            $this->success(__('Reset password successful'));
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 发送短信
     */
    public function sendcaptcha()
    {
        $mobile = $this->request->request("mobile");
        $type= $this->request->request("type");
        if (!preg_match("/^1[34578]{1}\d{9}$/", $mobile)) {
            $this->outputError('请输入正确的手机号');
        }
        $captch=mt_rand(1000, 9999);
//        $captch='111111';
        $ret = Sms::send($mobile,$captch,$type);
        if ($ret) {

            $target = "http://139.196.234.129:8888/sms.aspx";
            $time=date('Y-m-d H:i:s');
//替换成自己的测试账号,参数顺序和wenservice对应
            $post_data = "userid=24161&account=hszgzh123&password=hszgzh123&mobile=$mobile&sendTime=$time&action=send&content=".rawurlencode("您的验证码是：$captch.如需帮助请联系客服【红十字医院】");
            $gets = $this->Post($post_data, $target);
            if($gets) {
                $this->outputData('发送成功');
            }else{
                $this->outputError('发送失败');
            }
        }   else{
            $this->outputError('发送失败');
        }
    }
    function Post($curlPost,$url){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curlPost);
        $return_str = curl_exec($curl);
        curl_close($curl);
        return $return_str;
    }
    /**
     * 获取账户明细
     */
    public function logList(){
        $user=$this->auth->getUser();
        if(!$user){
            $this->outputError('请登录');
        }
        $type=$this->request->request('type');
        $uid= $user->id;
        $where=['uid'=>$uid];
        if($type){
            $where['type']=$type;
        }
        $logList=Db('bonus_log')->where($where)->select();
        foreach ($logList as &$value){
            $value['get_time']=$this->getDate($value['get_time']);
            $value['mobile']=Db('user')->where('fromid',$value['uid'])->value('mobile');
        }
        $this->outputData($logList);
    }
    /**
     * 获取优惠券明细
     */
    public function getTicketList(){
        $user=$this->auth->getUser();
        if(!$user){
            $this->outputError('请登录');
        }
        $uid= $user->id;
        $logList=Db('user_ticket')->where('uid',$uid)->select();
        foreach ($logList as &$value){
            $value['get_time']=$this->getDate($value['get_time']);
        }
        $this->outputData($logList);
    }
}
