<?php

return [
    'Ticket_name' => '优惠劵名称',
    'Price'       => '优惠券金额',
    'Effect'      => '有效期',
    'Create_time' => '发放日期',
    'Status'      => '状态 1正常2作废'
];
