<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Config;

/**
 * 控制台
 *
 * @icon fa fa-dashboard
 * @remark 用于展示当前系统中的统计数据、统计报表及重要实时数据
 */
class Dashboard extends Backend
{

    /**
     * 查看
     */
    public function index()
    {
        $this->view->assign([
            'todayusers'        =>Db('user')->where(['jointime'=>['gt',strtotime(date('Y-m-d').'00:00:00')]])->count(),
            'todayghnums'       => Db('order')->where(['ordertime'=>['gt',strtotime(date('Y-m-d').'00:00:00')],'ordertype'=>1])->count(),
            'todayghmoney'       => Db('order')->where(['ordertime'=>['gt',strtotime(date('Y-m-d').'00:00:00')],'ordertype'=>1])->sum('orderprice'),
            'todaytjnums' => Db('order')->where(['ordertime'=>['gt',strtotime(date('Y-m-d').'00:00:00')],'ordertype'=>2])->count(),
            'todaytjmoney'  => Db('order')->where(['ordertime'=>['gt',strtotime(date('Y-m-d').'00:00:00')],'ordertype'=>2])->sum('orderprice'),
            'users'   =>Db('user')->count(),
            'ghnums'       => Db('order')->where(['ordertype'=>1])->count(),
            'ghmoney'    => Db('order')->where(['ordertype'=>1])->sum('orderprice'),
            'tjnums'         => Db('order')->where(['ordertype'=>2])->count(),
            'tjmoney'         => Db('order')->where(['ordertype'=>2])->sum('orderprice'),
        ]);

        return $this->view->fetch();
    }

}
