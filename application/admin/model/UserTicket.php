<?php

namespace app\admin\model;

use think\Model;


class UserTicket extends Model
{

    

    

    // 表名
    protected $name = 'user_ticket';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'get_time_text'
    ];
    

    



    public function getGetTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['get_time']) ? $data['get_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setGetTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
